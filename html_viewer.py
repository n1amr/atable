from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QTextEdit


class HTMLViewer(QDialog):
    def __init__(self, html):
        super(HTMLViewer, self).__init__()
        layout = QVBoxLayout()
        self.setLayout(layout)

        self.html_source = QTextEdit()
        self.web_engine = QWebEngineView()
        layout.addWidget(self.html_source)
        layout.addWidget(self.web_engine)

        self.html_source.textChanged.connect(self.update_html_view)
        self.html_source.setPlainText(html)

    def update_html_view(self):
        self.web_engine.setHtml(self.html_source.toPlainText())
        self.html_source.setFocus()
