class ATable(object):
    def __init__(self, jinja_env, table=[[]], header=[],
                 style_class='', cell_style_class='',
                 header_cells_style_class=''):
        super(ATable, self).__init__()
        self.template = jinja_env.get_template('table.html')
        self.properties = {}
        self.update_properties(table=table, header=header,
                               style_class=style_class,
                               cell_style_class=cell_style_class,
                               header_cells_style_class=header_cells_style_class)

    def update_properties(self, **kwargs):
        self.properties.update(kwargs)

    def to_html(self):
        return self.template.render(self.properties)
