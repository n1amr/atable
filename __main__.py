import sys

from PyQt5.QtWidgets import QApplication
from jinja2.environment import Environment
from jinja2.loaders import PackageLoader

from html_viewer import HTMLViewer
from template_wrappers.a_table import ATable


def main():
    jinja_env = Environment(loader=PackageLoader('template_wrappers', 'html/templates'))

    table = ATable(jinja_env=jinja_env,
                   table=["one 1".split(),
                          "two 2".split(),
                          "three 3".split(), ],
                   header=['name', 'number'],
                   style_class='blue_borders',
                   header_cells_style_class='blue_borders', )

    html = table.to_html()

    app = QApplication(sys.argv)
    html_viewer = HTMLViewer(html)
    html_viewer.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
